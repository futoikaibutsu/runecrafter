package com.jackgriffiths.scripts.xrunecrafter;

import java.util.ArrayList;

import com.jackgriffiths.scripts.xrunecrafter.methods.BankHandler;
import com.jackgriffiths.scripts.xrunecrafter.methods.CraftHandler;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.Strategy;

@ScriptManifest(author = "Konata", category = Category.OTHER, description = "Crafts Nearly every rune on PKHonor", name = "XRunecrafter", servers = {"PKHonor"}, version = 0.01D)
public class XRunecrafter extends Script {
    public static final int ESS_ID = 1436;
    private final ArrayList<Strategy> strategies = new ArrayList<Strategy>();

    public boolean onExecute() {
        strategies.add(new BankHandler());
        strategies.add(new CraftHandler());
        provide(strategies);
        return true;
    }

    public void onFinish() {
        System.out.println("Thanks for running this glorious script!");
    }
}