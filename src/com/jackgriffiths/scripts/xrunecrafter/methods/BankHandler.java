package com.jackgriffiths.scripts.xrunecrafter.methods;

import com.jackgriffiths.scripts.xrunecrafter.XRunecrafter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.api.methods.Bank;
import org.rev317.api.methods.Inventory;
import org.rev317.api.methods.SceneObjects;
import org.rev317.api.wrappers.hud.Item;
import org.rev317.api.wrappers.scene.SceneObject;

public class BankHandler implements Strategy {
    private final int BANK_ID = 2213;

    public boolean activate() {
        return Inventory.getCount(XRunecrafter.ESS_ID) == 0;
    }

    public void execute() {
        SceneObject bank = SceneObjects.getClosest(BANK_ID);
        if (bank != null) {
            if (bank.isOnScreen()) {
                if (!Bank.isOpen()) {
                    bank.interact("Use-quickly");
                    Time.sleep(700);
                }
                if (Bank.isOpen()) {
                    Item ess = Bank.getItem(1436);
                    if (ess != null) {
                        Bank.withdraw(1436, 0, 300);
                        Bank.close();
                    }
                }
            }
        }
    }
}

