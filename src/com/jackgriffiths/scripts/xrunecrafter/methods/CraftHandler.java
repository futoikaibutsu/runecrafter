package com.jackgriffiths.scripts.xrunecrafter.methods;

import com.jackgriffiths.scripts.xrunecrafter.XRunecrafter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.api.methods.Camera;
import org.rev317.api.methods.Inventory;
import org.rev317.api.methods.SceneObjects;
import org.rev317.api.wrappers.scene.SceneObject;

/*
To finish: Custom methods for Fire, Earth
To consider: Blood, Soul
 */

public class CraftHandler implements Strategy {
    private final int[] ALTAR_IDS = {2478, 2479, 2480, 2481, 2482, 2483, 2484, 2485, 2486, 2487, 2488};

    public boolean activate() {
        return Inventory.getCount(XRunecrafter.ESS_ID) >= 1;
    }

    public void execute() {
        SceneObject altar = SceneObjects.getClosest(ALTAR_IDS);
        if (altar != null) {
            if (!altar.isOnScreen()) {
                Camera.turnTo(altar);
                Time.sleep(500, 780);
            }
            if (altar.isOnScreen()) {
                altar.interact("Craft-rune");
                Time.sleep(250);
            }
        }
    }
}